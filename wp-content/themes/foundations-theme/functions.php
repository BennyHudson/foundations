<?php

	add_action('after_setup_theme', 'foundations_setup');
	function foundations_setup() {
		add_action('wp_enqueue_scripts', 'sprout_dashicons');
		add_action('init', 'foundation_testimonials');
		add_action('init', 'register_my_menus');
		add_action('init', 'flush_rewrite_rules');
		add_filter('wp_mail_from_name', 'new_mail_from_name');
		add_theme_support( 'post-thumbnails' );
		add_action( 'admin_init', 'my_theme_add_editor_styles' );
	}

	function my_theme_add_editor_styles() {
    	add_editor_style( 'admin.css' );
	}

// Custom Post Types

	function foundation_testimonials() {
		$labels = array (
			'name'					=> _x('Testimonials', 'post type general name'),
			'singular_name'			=> _x('Testimonial', 'post type singular name'),
			'add_new'				=> _x('Add new testimonial', 'book'),
			'add_new_item'			=> ('Add New Testimonial'),
			'edit_item'				=> ('Edit Testimonial'),
			'new_item'				=> ('New Testimonial'),
			'all_items'				=> ('All Testimonials'),
			'view_item'				=> ('View Testimonials'),
			'search_items'			=> ('Search Testimonials'),
			'not_found'				=> ('No Testimonials found'),
			'not_found_in_trash'	=> ('No Testimonials found in trash'),
			'parent_item_colon'		=> '',
			'menu_name'				=> 'Testimonials'
		);
		$args = array(
			'labels'		=> $labels,
			'description'	=> 'Holds all the Foundations Testimonials',
			'public'		=> true,
			'menu_position'	=> 19,
			'menu_icon'		=> 'dashicons-format-status',
			'supports'		=> array('title', 'editor', 'excerpt', 'revisions', 'page-attributes'),
			'has_archive'	=> true
		);
		register_post_type('Testimonials', $args);
	}

// Menus

	function register_my_menus() {
	  register_nav_menus(
		array( 'mobile-menu' => __( 'Mobile Menu' ),
			   'footer-menu' => __( 'Footer Menu' ),
			   'header-menu' => __( 'Header Menu' )
			 )
	  );
	}

// Email Settings

	function new_mail_from_name($old) {
	 return 'Foundations Physio';
	}

	function my_login_logo() { ?>
	    <style type="text/css">
	        body.login div#login h1 a {
	            background-image: url(<?php echo get_bloginfo( 'template_directory' ) ?>/images/large-logo.png);
				display: block;
				background-size: contain;
	        }
	    </style>
	<?php }
	add_action( 'login_enqueue_scripts', 'my_login_logo' );
	function my_login_logo_url() {
	    return get_bloginfo( 'url' );
	}
	add_filter( 'login_headerurl', 'my_login_logo_url' );
	// First, create a function that includes the path to your favicon
	function add_favicon() {
	  	$favicon_url = get_stylesheet_directory_uri() . '/images/favicons/favicon.ico';
		echo '<link rel="shortcut icon" href="' . $favicon_url . '" />';
	}
	  
	// Now, just make sure that function runs when you're on the login page and admin pages  
	add_action('login_head', 'add_favicon');
	add_action('admin_head', 'add_favicon');
?>
