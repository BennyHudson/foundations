			<footer>
				<nav>
					<?php if(!is_page_template('template-holding.php')) { ?>
						<section class="container">
							<?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>
						</section>
					<?php } ?>
				</nav>
				<section class="container">
					<aside>
						<p><i class="fa fa-facebook-square"></i> <a href="https://www.facebook.com/foundationsphysio" target="_blank">foundationsphysio</a>
					</aside>
					<aside>
						<p>
							T: 07817 495 791<br>
							E: <a href="mailto:adam@foundationsphysio.com">adam@foundationsphysio.com</a>
						</p>
					</aside>
					<aside>
						<p>
							Website by <a href="http://ben-hudson.co.uk/" target="_blank">The Twenty Six</a><br>
							&copy; Foundations Physio <?php echo date('Y'); ?>
						</p>
					</aside>
				</section>
			</footer>
		</section>
	</section>
</body>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/jquery-1.8.2.min.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/production.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/js/font-awesome/css/font-awesome.min.css">
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-35663434-4', 'auto');
  ga('send', 'pageview');

</script>
<?php wp_footer() ?>
</html>
