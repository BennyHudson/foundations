<?php
	// Template Name: Holding Page
?>
<?php get_header(); ?>
	<section class="holding-page">
		<section class="container">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/holding-logo.png" alt="<?php wp_title('|'); ?>">
			<h2>Website Coming Soon</h2>
			<p>We're busy creating a beautiful new website for Foundations Physio, in the mean time, get in touch with us on <a href="https://www.facebook.com/foundationsphysio" target="_blank">Facebook</a> or email us at <a href="mailto:adam@foundationsphysio.com">adam@foundationsphysio.com</a>.
		</section>
	</section>
<?php get_footer(); ?>
