<?php if(is_page(13) || is_page(11)) { ?>
	<section class="widget contact">
		<h2>Get in touch</h2>
		<ul>
			<li><a href="https://www.facebook.com/foundationsphysio" target="_blank"><i class="fa fa-facebook-square"></i> <span>foundationsphysio</span></a></li>
			<li><a href="tel:07817 495 791"><i class="fa fa-phone-square"></i> <span>07817 495 791</span></a></li>
			<li><a href="mailto:adam@foundationsphysio.com"><i class="fa fa-envelope-square"></i> <span>adam@foundationsphysio.com</span></a></li>
		</ul>
	</section>
<?php } else { ?>
	<section class="widget">
		<h2>
			<?php
				$parent_title = get_the_title($post->post_parent);
				echo $parent_title;
			?>
		</h2>
		<?php
            if($post->post_parent)
            $children = wp_list_pages("title_li=&child_of=".$post->post_parent."&echo=0");
                else
                $children = wp_list_pages("title_li=&child_of=".$post->ID."&echo=0");
                if ($children) { ?>
                    <ul>
                        <?php echo $children; ?>
                    </ul>
        <?php } ?>
	</section>
<?php } ?>
