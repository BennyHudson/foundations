<?php
	//Template Name: Landing Page
?>
<?php get_header(); ?>
	<section class="landing-feature" style="background-image: url('<?php the_field('feature_image'); ?>');">
		<section class="container">
			<section class="vert-fix">
				<img src="<?php the_field('feature_image'); ?>" class="mobile-feature">
				<h2><?php the_field('image_text'); ?></h2>
			</section>
		</section>
	</section>
	<section class="container">
		<section class="landing-content">
			<?php the_content(); ?>
		</section>
		<section class="landing-testimonial">
			<?php
	            $queryObject = new WP_Query( 'post_type=testimonials&posts_per_page=1&orderby=rand' );
	            if ($queryObject->have_posts()) {
	        ?>
	        	<?php
	                while ($queryObject->have_posts()) {
	                $queryObject->the_post();
	            ?>
	            	<?php the_excerpt(); ?>
	            <?php } ?>
	        <?php } ?>
	    </section>
	</section>
<?php get_footer(); ?>
