<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/modernizr-latest.js" type="text/javascript"></script>
<script type="text/javascript" src="http://fast.fonts.net/jsapi/24f07956-c74c-4ea4-9940-2db1bea222fc.js"></script>
<?php get_template_part('includes/build', 'meta'); ?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width"/>  
<title><?php wp_title('|'); ?></title>
<?php wp_head(); ?>
</head>
<body>
<section class="page-content">
	<section class="footer-fix">
		<?php if(!is_page_template('template-holding.php')) { ?>
			<nav class="mobile-nav">
				<?php wp_nav_menu( array( 'theme_location' => 'mobile-menu' ) ); ?>
			</nav>
			<a href="#" class="mob-nav-trigger">Menu</a>
			<header>
				<section class="container">
					<aside>
						<?php 
							$title = get_bloginfo('name');
							$tagline = get_bloginfo('description');
							$url = get_bloginfo('url');
						?>
						<?php if(is_front_page()) { ?>
							<h1><a href="<?php echo $url; ?>" title="<?php echo $title; ?> | <?php echo $tagline; ?>"><?php echo $title; ?> | <?php echo $tagline; ?></a></h1>
						<?php } else { ?>
							<h2><a href="<?php echo $url; ?>" title="<?php echo $title; ?>"><?php echo $title; ?></a></h2>
						<?php } ?>
					</aside>
					<aside>
						<!--Start Menu-->
							<nav>
								<?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
							</nav>
						<!--End Menu-->	
					</aside>
				</section>
			</header>
		<? } ?>
