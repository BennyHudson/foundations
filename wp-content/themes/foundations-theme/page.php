<?php get_header(); ?>
	<section class="container">
		<aside class="sidebar">
			<?php get_sidebar(); ?>
		</aside>
		<aside class="main-content">
		<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post(); ?>
			<section class="feature">
				<h1 class="page-title"><?php the_title(); ?></h1>
				<?php the_post_thumbnail('full'); ?>
			</section>
			<section class="content">
				<?php the_content(); ?>
			</section>
		<?php endwhile; ?>
		<?php else: ?>
            <?php get_template_part('partials/template', 'error'); ?>
        <?php endif; ?>
		</aside>
	</section>
<?php get_footer(); ?>
