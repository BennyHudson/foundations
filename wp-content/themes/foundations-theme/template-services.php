<?php
	//Template Name: Services
?>
<?php get_header(); ?>
	<section class="container">
		<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post(); ?>
			<?php if(get_field('services')) : ?>
				<aside class="sidebar">
					<section class="widget services">
						<h2>What we do</h2>
						<ul>
							<?php while(the_repeater_field('services')) : ?>
								<li><a href="#"><?php the_sub_field('service_title'); ?></a></li>
							<?php endwhile; ?>
						</ul>
					</section>
				</aside>
				<aside class="main-content">
					<section class="feature">
						<h1 class="page-title"><?php the_title(); ?></h1>
						<?php the_post_thumbnail('full'); ?>
					</section>
					<section class="content">
						<?php the_content(); ?>
						<?php while(the_repeater_field('services')) : ?>
							<section class="service-content">
								<h3><?php the_sub_field('service_title'); ?></h3>
								<?php the_sub_field('service_content'); ?>
								<a href="#" class="back-up button">Back to top</a>
							</section>
						<?php endwhile; ?>
					</section>
				</aside>
			<?php endif; ?>
		<?php endwhile; ?>
		<?php else: ?>
	        <?php get_template_part('partials/template', 'error'); ?>
	    <?php endif; ?>
    </section>
<?php get_footer(); ?>
