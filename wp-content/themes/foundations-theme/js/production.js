/*global jQuery */
/*jshint multistr:true browser:true */
/*!
* FitVids 1.0
*
* Copyright 2013, Chris Coyier - http://css-tricks.com + Dave Rupert - http://daverupert.com
* Credit to Thierry Koblentz - http://www.alistapart.com/articles/creating-intrinsic-ratios-for-video/
* Released under the WTFPL license - http://sam.zoy.org/wtfpl/
*
* Date: Thu Sept 01 18:00:00 2011 -0500
*/

(function( $ ){

  "use strict";

  $.fn.fitVids = function( options ) {
    var settings = {
      customSelector: null
    };

    if(!document.getElementById('fit-vids-style')) {

      var div = document.createElement('div'),
          ref = document.getElementsByTagName('base')[0] || document.getElementsByTagName('script')[0],
          cssStyles = '&shy;<style>.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}</style>';

      div.className = 'fit-vids-style';
      div.id = 'fit-vids-style';
      div.style.display = 'none';
      div.innerHTML = cssStyles;

      ref.parentNode.insertBefore(div,ref);

    }

    if ( options ) {
      $.extend( settings, options );
    }

    return this.each(function(){
      var selectors = [
        "iframe[src*='player.vimeo.com']",
        "iframe[src*='youtube.com']",
        "iframe[src*='youtube-nocookie.com']",
        "iframe[src*='kickstarter.com'][src*='video.html']",
        "object",
        "embed"
      ];

      if (settings.customSelector) {
        selectors.push(settings.customSelector);
      }

      var $allVideos = $(this).find(selectors.join(','));
      $allVideos = $allVideos.not("object object"); // SwfObj conflict patch

      $allVideos.each(function(){
        var $this = $(this);
        if (this.tagName.toLowerCase() === 'embed' && $this.parent('object').length || $this.parent('.fluid-width-video-wrapper').length) { return; }
        var height = ( this.tagName.toLowerCase() === 'object' || ($this.attr('height') && !isNaN(parseInt($this.attr('height'), 10))) ) ? parseInt($this.attr('height'), 10) : $this.height(),
            width = !isNaN(parseInt($this.attr('width'), 10)) ? parseInt($this.attr('width'), 10) : $this.width(),
            aspectRatio = height / width;
        if(!$this.attr('id')){
          var videoID = 'fitvid' + Math.floor(Math.random()*999999);
          $this.attr('id', videoID);
        }
        $this.wrap('<div class="fluid-width-video-wrapper"></div>').parent('.fluid-width-video-wrapper').css('padding-top', (aspectRatio * 100)+"%");
        $this.removeAttr('height').removeAttr('width');
      });
    });
  };
// Works with either jQuery or Zepto
})( window.jQuery || window.Zepto );

$(document).ready(function() {
    var footerHeight = $('footer').outerHeight();
    $('.footer-fix').css('padding-bottom', footerHeight);

	$('.container').fitVids();
	$('.mob-nav-trigger').click(function(e) {
		e.preventDefault();
		$('nav.mobile-nav').slideToggle();
	});
	$('.widget.services').find('li a').each( function(i){
      var postnum = i + 1; // remove any possible overloaded "+" operator ambiguity
      $(this).attr('data-section', 'section-' + postnum);
    });
    $('.content').find('.service-content').each( function(i){
      var postnum = i + 1; // remove any possible overloaded "+" operator ambiguity
      $(this).attr('id', 'section-' + postnum);
    });
    $('.widget.services li a').click(function(e) {
    	var scrolltarget = $(this).data('section');
    	var target = $('#' + scrolltarget);
    	e.preventDefault();
    	$('html,body').animate({
            scrollTop: target.offset().top -60
        }, 1000);
    });
    $('.back-up').click(function(e) {
    	e.preventDefault();
    	var target = $('header');
    	$('html,body').animate({
            scrollTop: target.offset().top -60
        }, 1000);
    });
});
$(window).scroll(function() {
	var headerHeight = $('header').outerHeight();
	if ($(this).scrollTop() > headerHeight) {
		var containerWidth = $('.container').outerWidth();
		$('.widget').addClass('fixed').css('width', containerWidth / 4);
	} else {
		$('.widget').removeClass('fixed').css('width', '100%');
	}
});
$(window).resize(function() {
    var footerHeight = $('footer').outerHeight();
    $('.footer-fix').css('padding-bottom', footerHeight);
});
