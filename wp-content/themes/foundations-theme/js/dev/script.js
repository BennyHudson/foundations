$(document).ready(function() {
    var footerHeight = $('footer').outerHeight();
    $('.footer-fix').css('padding-bottom', footerHeight);

	$('.container').fitVids();
	$('.mob-nav-trigger').click(function(e) {
		e.preventDefault();
		$('nav.mobile-nav').slideToggle();
	});
	$('.widget.services').find('li a').each( function(i){
      var postnum = i + 1; // remove any possible overloaded "+" operator ambiguity
      $(this).attr('data-section', 'section-' + postnum);
    });
    $('.content').find('.service-content').each( function(i){
      var postnum = i + 1; // remove any possible overloaded "+" operator ambiguity
      $(this).attr('id', 'section-' + postnum);
    });
    $('.widget.services li a').click(function(e) {
    	var scrolltarget = $(this).data('section');
    	var target = $('#' + scrolltarget);
    	e.preventDefault();
    	$('html,body').animate({
            scrollTop: target.offset().top -60
        }, 1000);
    });
    $('.back-up').click(function(e) {
    	e.preventDefault();
    	var target = $('header');
    	$('html,body').animate({
            scrollTop: target.offset().top -60
        }, 1000);
    });
});
$(window).scroll(function() {
	var headerHeight = $('header').outerHeight();
	if ($(this).scrollTop() > headerHeight) {
		var containerWidth = $('.container').outerWidth();
		$('.widget').addClass('fixed').css('width', containerWidth / 4);
	} else {
		$('.widget').removeClass('fixed').css('width', '100%');
	}
});
$(window).resize(function() {
    var footerHeight = $('footer').outerHeight();
    $('.footer-fix').css('padding-bottom', footerHeight);
});
