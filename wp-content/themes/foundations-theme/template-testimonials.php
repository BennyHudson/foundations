<?php
	//Template Name: Testimonials
?>
<?php get_header(); ?>
	<section class="container">
		<aside class="sidebar">
			<?php get_sidebar(); ?>
		</aside>
		<aside class="main-content">
		<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post(); ?>
			<section class="feature">
				<h1 class="page-title"><?php the_title(); ?></h1>
				<?php the_post_thumbnail('full'); ?>
			</section>
			<section class="content">
				<?php the_content(); ?>
					<?php
                        $queryObject = new WP_Query( 'post_type=testimonials&posts_per_page=-1' );
                        if ($queryObject->have_posts()) {
                    ?>
	                    <ul class="testimonials">
		                    <?php
		                        while ($queryObject->have_posts()) {
		                        $queryObject->the_post();
		                    ?>
		                    	<li>
		                    		<h3><?php the_title(); ?></h3>
		                    		<?php the_content(); ?>
		                    	</li>
		                    <?php } ?>
						</ul>
					<?php } ?>
			</section>
		<?php endwhile; ?>
		<?php else: ?>
            <?php get_template_part('partials/template', 'error'); ?>
        <?php endif; ?>
		</aside>
	</section>
<?php get_footer(); ?>
